﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TP1_RPG.GameObjects;

namespace TP1_RPG
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello fellow player ! What's your name ?");
            Player player = new Player(Console.ReadLine(), 10, 10, 5, 5, new List<Skill> { new Skill("autohit", 2, 0, 0),
                new Skill("fireball", 4, 0, 2) }, 0, Player.Sexe.Male);
            Console.WriteLine("Fine " + player.name + " ! Welcome to a brand new world.");
            Console.WriteLine("You have " + player.competences.Count + " attacks at your disposition and you are level " + player.GetLevel() + ".");

            Console.Write("Are you ready ? Press a key to start !");
            Console.ReadLine();

            Console.Clear();
            scenarios.Scenario1.Run(ref player);

            Console.Read();
        }
    }
}
