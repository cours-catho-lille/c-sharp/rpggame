﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TP1_RPG.GameObjects;

namespace TP1_RPG
{
    public class FightManager
    {
        public static void Fight(Player player, Creature creature)
        {
            Console.WriteLine();
            Console.WriteLine("You are attacking first, what do you want to do ?");
            Console.WriteLine();
            while (creature.currentHp > 0)
            {
                player.Attack(player.ChooseSkill(), creature);
                if(creature.currentHp > 0)
                {
                    creature.Attack(creature.ChooseSkill(), player);
                    if (player.currentHp <= 0)
                    {
                        player.Die();
                    }
                }
            }
        }
    }
}
