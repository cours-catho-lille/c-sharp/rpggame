﻿using System;
using System.Collections.Generic;

namespace TP1_RPG.GameObjects
{
    public class Creature : Being
    {
        public int xpReward { get; set; }
        public List<Item> drops;
        public int money = 0;
        public string emote;

        public Creature(string name, int currentHp, int maxHp, int currentMana, int maxMana, List<Skill> competences, int xpReward, List<Item> drops, int money = 0, string emote = "") 
            : base(name, currentHp, maxHp, currentMana, maxMana, competences)
        {
            this.name = name;
            this.currentHp = currentHp;
            this.maxHp = maxHp;
            this.currentMana = currentMana;
            this.maxMana = maxMana;
            this.competences = competences;
            this.xpReward = xpReward;
            this.drops = drops;
            this.money = money;
            this.emote = emote;
        }

        public Skill ChooseSkill()
        {
            foreach (Skill skill in this.competences)
            {
                if (skill.manaCost <= this.currentMana){
                    return skill;
                }
            }
            return competences[0];
        }
    }
}
